libsvn-dump-perl (0.08-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libsvn-dump-perl: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 14:45:02 +0100

libsvn-dump-perl (0.08-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.08.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Mar 2020 14:15:52 +0100

libsvn-dump-perl (0.07-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.07.
  * Update build dependencies.
  * Update debian/upstream/metadata.
  * Drop fix-pod-spelling.patch, applied upstream.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Fri, 31 Jan 2020 18:11:53 +0100

libsvn-dump-perl (0.06-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 May 2015 21:58:48 +0200

libsvn-dump-perl (0.06-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 0.06
  * Update years of upstream copyright
  * Added build-dependency on Module::Build 0.38
  * Bump Standards-Version to 3.9.4 (no change)
  * Do not ship README, its contents are redundant
  * Update and forward fix-pod-spelling.patch
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Fri, 20 Sep 2013 12:27:26 +0200

libsvn-dump-perl (0.05-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Make examples executable.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Jonathan Yu ]
  * New upstream release
  * Rewrite control description
  * Add myself to Uploaders and Copyright
  * Bump to debhelper compat 8
  * Use new 3.0 (quilt) source format
  * Standards-Version 3.9.1 (no changes)
  * Refresh copyright information
  * Use short debhelper rules format
  * Add a patch to fix POD spelling errors

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 20 Mar 2011 12:12:38 -0400

libsvn-dump-perl (0.04-1) unstable; urgency=low

  * Initial Release. (Closes: #489235)

 -- Edi Stojicevic <estojicevic@debianworld.org>  Fri, 04 Jul 2008 11:06:04 +0100
